* 1.2.0

    * sylph-searcher: the phrase search feature was added.
    * sylph-searcher: Cc is also searched with To.
    * sylph-searcher: folder and attach were added to the search criteria.
    * sylph-searcher: tooltips were added.
    * syldbimport: the option '--delete' which recursively deletes folders
      from DB was added.
    * the new column 'attach_num' will be added to msginfo table if not exist.
    * Sylph-Searcher can build without MeCab if you don't require Japanese
      support.

* 1.1.1

    * sylph-searcher: the default database name 'sylph' is set on first run.
    * sylph-searcher: win32: port 25432 is used as the default port.
    * sylph-searcher: The problem that the dialog didn't become transient was
      fixed.
    * the command-line option parser was fixed so that --debug option can be
      used with other options.

* 1.1.0

    * syldbimport: skip inserting if base64-encoded lines appeared in the
      body.
    * syldbimport: the option '--exclude' was added.
    * sylph-searcher: win32: only show console window when --debug option is
      specified.
    * made the help message friendly.

* 1.0.0

    * SQL scripts are also installed.
    * README: updated.
    * sylph-searcher: win32: the bug that alert dialog became invisible when
      main window was minimized was fixed.

* 1.0beta1

    * syldbimport: skip queue and trash folders.
    * sylph-searcher: menubar was added.
    * sylph-searcher: strings are translated.
    * sylph-searcher: configuration dialog was added.
    * sylph-searcher: date condition was added.
    * sylph-searcher: the status label that shows search result was added.
    * sylph-searcher: error dialog is displayed if connection to database
      failed.
    * sylph-searcher: the option of maximum display number of result was
      added.
    * README: updated.
    * The license has been changed to the BSD license, and the copyright
      holder became Sylpheed Development Team.

* 0.5.0

    * sql/create.sql: the index was added on msg_folderinfo.msg_sid.
    * configure.ac: '--with-pgsql' and '--with-libsylph' were added.
    * syldbimport: only delete from msg_folderinfo if '-n' is specified.
    * sylph-searcher: keywords are now highlighted.
    * sylph-searcher: clear button was added.

* 0.4.0

    * syldbimport: The recursive import was added.
    * syldbimport: '-v' option was added.
    * Win32: sylph-searcher: The location of sylpheed.exe is automatically
      searched from registry.

* 0.3.0

    * syldbimport: The option not to delete nonexistent messages was added.
    * syldbimport: Multi-byte strings in the arguments are now handled
      correctly.
    * Win32: SQL scripts are included.

* 0.2.0

    * syldbimport: Implemented difference import.
    * syldbimport: Implemented cleanup of removed messages.
    * DB connection info was abstracted.
    * Create.sql was updated.
    * Message-Id is checked when importing.
    * Messages are sorted by number when importing.
    * Count statistics.

* 0.1.0

    * Initial release.
