/* Sylph-Searcher - full-text search program for Sylpheed
 * Copyright (C) 2007 Sylpheed Development Team
 */

#ifndef __COMMON_H__
#define __COMMON_H__

typedef struct _DBInfo		DBInfo;
typedef struct _AppConfig	AppConfig;
typedef struct _Options		Options;

struct _DBInfo
{
	gpointer dbdata;
};

struct _AppConfig
{
	gchar *dbname;
	gchar *hostname;
	gushort port;
	gchar *user;
	gchar *pass;

	gint res_limit;
};

struct _Options
{
	gchar *dbname;
	gchar *hostname;
	gushort port;
	gchar *user;
	gchar *pass;
	gboolean no_remove;
	gboolean recursive;
	gboolean verbose;
	gchar *mecab_encoding;
	GSList *exclude_list;
	gboolean delete_folder;
};

gchar *sql_escape_str(DBInfo *conn, const gchar *str);
gchar *sql_escape_like_str(DBInfo *conn, const gchar *str);
gchar *get_wakachi_text(const gchar *str);
GSList *get_phrase_text(const gchar *str);

gint parse_cmdline(gint argc, gchar *argv[],
		   gchar **dbname, gchar **hostname, gushort *port,
		   gchar **user, gchar **pass);
gint parse_cmdline_options(gint argc, gchar *argv[], Options *options);
gboolean cmdline_has_option(gint argc, gchar *argv[], const gchar *opt);

gint read_config(AppConfig *config);
gint write_config(AppConfig *config);

DBInfo *db_connect(const gchar *dbname, const gchar *hostname, gushort port,
		   const gchar *user, const gchar *pass);
gint db_disconnect(DBInfo *conn);
gint db_check_connection(DBInfo *conn);
gint db_check_tables(DBInfo *conn);
void db_error_message(DBInfo *conn, const gchar *msg);

gint db_start_transaction(DBInfo *conn);
gint db_end_transaction(DBInfo *conn);
gint db_abort_transaction(DBInfo *conn);

gint db_exec_command(DBInfo *conn, const gchar *query);

gint db_get_sid_from_msgid(DBInfo *conn, const gchar *msgid, gulong *sid);
gint db_get_sid_from_folderinfo(DBInfo *conn, const gchar *folder_id,
				guint msgnum, gulong *sid);

gint db_delete_msg_from_folderinfo(DBInfo *conn, const gchar *folder_id,
				   guint msgnum);
gint db_delete_folderinfo_recursive(DBInfo *conn, const gchar *folder_id);
gint db_is_sid_exist_in_folderinfo(DBInfo *conn, gulong sid);
gint db_delete_msg(DBInfo *conn, gulong sid);
gint db_delete_folder(DBInfo *conn, const gchar *folder_id, gboolean recursive);

gchar *db_get_body(DBInfo *conn, gulong sid);

#endif /* __COMMON_H__ */
