/* Sylph-Searcher - full-text search program for Sylpheed
 * Copyright (C) 2007-2008 Sylpheed Development Team
 */

#include <gtk/gtk.h>
#include "common.h"

gint searcher_config(AppConfig *config, GtkWindow *parent);
